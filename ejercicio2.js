let vector = [1,2,3,4,5]
//a. Imprimir en la consola cada valor usando "for"
for (i=0,i== vector.length;i++;){
    console.log('Elemento numero['+i+']: '+vector[i]);
}
//b. Idem al anterior usando "forEach"
vector.forEach(elemento=>function(elemento){
    console.log(elemento)
})
//c. Idem al anterior usando "map"
vector.map(elemento=>function(elemento){
    console.log(elemento)
})
//d. Idem al anterior usando "while"
let i=0;
while(i==vector.length){
    console.log('Elemento numero['+i+']: '+vector[i]);
    i++;
}

///e. Idem al anterior usando "for..of"
for (let elemento of vector){
    console.log(elemento)
}