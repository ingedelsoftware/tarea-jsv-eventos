// a. Imprimir en la consola el vector
let vector = [1, 2, 3, 4, 5];
console.log(vector);
// b. Imprimir en la consola el primer y el último elemento del vector usando sus índices
console.log(vector[0]);
console.log(vector[vector.length - 1]);
// c. Modificar el valor del tercer elemento
if (vector.length >= 3 ){
    vector[2] = vector[2] +1;
}else{
    console.log("No hay tercer elemento");
}
// d.  Imprimir en la consola la longitud del vector
console.log(vector.length);

//e. Agregar un elemento al vector usando "push"
vector.push(6);

// f. Eliminar elemento del final e imprimirlo usando "pop"
let ultimo = vector.pop();
console.log(ultimo);

// g. Agregar un elemento en la mitad del vector usando "splice"
let mitad = vector.length%2==0?(vector.length)/2 :(vector.length+1)/2;
vector.splice(mitad, 0, 11);

// h. Eliminar el primer elemento usando "shift"
let primero = vector.shift();
//i. Agregar de nuevo el mismo elemento al inicio del vector usando "unshift"
vector.unshift(primero);